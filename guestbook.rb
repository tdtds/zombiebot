require 'sinatra'
require 'haml'

@@solution = '180+178-231-27'

get '/' do
	haml :index
end

post '/' do
	$stderr.puts request[:name]
	$stderr.puts request[:text]
	$stderr.puts request[:solution]
	haml :index
end

post '/solution' do
	@@solution = params[:sol]
	haml :index
end

__END__

@@index
%html
	%head
		%title<z0omie Spam 1.0
	%body
	%div{id: "posts"}
	Invalid solution
	%div{id: "form"}
		%form{method: "POST", style: "width:900px; margin:auto;"}
			%p
				%label{for: "name"}<Name
				%br
				%input{type: "text", id: "name", name: "name"}
			%p
				%label{for: "text"}<Text
				%br
				%textarea{name: "text", id: "text"}
			%p
				%label{for: "solution"}
					Spam protection: 
					%b<#{@@solution}
					%br
					%input{type: "text", id: "solution", name: "solution"}
			%input{type: "submit", id: "submit", name: "submit", value: "Post"}
